TEMPLATE = subdirs

SUBDIRS += \
    MainUI \
    InfraredLogic\
    CaptureImages\
    DataBaseRead \
    DataBaseInsert \
    IdentifyImages\
    ResultsAnalysis \
    SocketService \
    Encryption \
    CaptureUnderlying \
    ToUploadData \
    DataInterchange \
    ElectronicLicensePlate
